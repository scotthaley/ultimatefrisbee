var socket, userID, acceptedFriends;

function connectToServer(id, aFriends)
{
	if (window['io'] != undefined)
	{
		userID = id;
		acceptedFriends = aFriends.split(",");
		socket = io.connect("http://99.136.94.105", {port: 8000, transports:["websocket"]});
		socket.on("connect", onSocketConnection);
		socket.on("update client", onUpdateClients);
		socket.on("player status", onPlayerStatus);
		socket.on("found game", onFoundGame);
		socket.on("game started", onGameStarted);
		socket.on("searching", onGameSearching);

		queryFriends();
		setInterval(queryFriends, 5000);
	} else {
		console.log("Could not connect.");
		$("#serverStatusPanel").removeClass("panel-primary-solid").addClass("panel-danger-solid");
		$("#serverStatusText").text("Offline");
	}
}

function onPlayerStatus(data)
{
	for (var i = 0; i < acceptedFriends.length; i ++)
	{
		var userID = acceptedFriends[i];
		if (data[userID] == true)
		{
			$("#chatLiItem-" + userID).removeClass("idle");
			$("#chatIcon-" + userID).addClass("text-online");
		} else {
			$("#chatLiItem-" + userID).addClass("idle");
			$("#chatIcon-" + userID).removeClass("text-online");
		}
	}
}

function onGameSearching(data)
{
	if (data.gameType == 1)
	{
		$("#1NormalBig").text("Searching");
		$("#1NormalSmall").text("Looking for " + data.waiting + " player...");

		$("#3Normal").removeClass("panel-warning-solid").addClass("panel-disabled-solid");
		$("#1Ranked").removeClass("panel-primary-solid").addClass("panel-disabled-solid");
		$("#3Ranked").removeClass("panel-info-solid").addClass("panel-disabled-solid");
	}
}

function searchGame(gameType)
{
	socket.emit("search game", {gameType: gameType});
	console.log("Searching for game...");
}

function acceptGame()
{
	socket.emit("accept game");
	console.log("Accepted game.");

	$("#gameFoundFooter").html("Waiting for other players to accept...");
}

function onGameStarted()
{
	window.location = "play.php";
}

function onFoundGame(data)
{
	var percent = 100 - data.timeout / 30 * 100;
	if (percent <= 100)
		$("#gameTimeoutBar").css("width", percent + "%");

	if (percent == 0)
		$("#gameFoundModal").modal("show");
	if (percent == 100)
		$("#gameFoundModal").modal("hide");

	console.log(percent);
}

function queryFriends()
{
	if (acceptedFriends.length  != 0)
	{
		socket.emit("query players", acceptedFriends);
	}
}

function onSocketConnection()
{
	console.log("Connected to server.");
	$("#serverStatusText").text("Online");

	socket.emit("new client", {id: userID});
}

function onUpdateClients(data)
{
	$("#playersOnline").text(data.count);
}



