
/*
Frisby Golf
By: Scott Haley
*/

var FIELD_WIDTH = 600;
var FIELD_HEIGHT = 300;
var GOAL_DISTANCE = 75;
var GOAL_SIZE = 40;

function ultimatefrisbee()
{
	var _this = this;

	_this.init = function(container, userID)
	{
		_this.userID = userID;

		_this._webGLManager = new webGLManager();
		_this._webGLManager.init(container);
		_this._webGLManager.registerUpdate(_this.update);
		_this.createGame();
		_this.secondPlayer = null;

		_this.currentGameMode = 0;

		_this.remotePlayers = [];

		_this._webGLManager.zoom = 0.5;

		_this._keyboardManager = new KeyboardManager();
		_this._keyboardManager.init();

		_this._mouseManager = new MouseManager(container);
		_this._mouseManager.init();

		_this.socket = io.connect("http://99.136.94.105", {port: 8000, transports:["websocket"]});
		_this.socket.on("connect", _this.onSocketConnection);
		_this.socket.on("player disconnect", _this.onPlayerDisconnect);
		_this.socket.on("new player", _this.onNewPlayer);
		_this.socket.on("update", _this.onServerUpdate);
		_this.socket.on("not in game", _this.notInGame);
	}

	_this.notInGame = function()
	{
		window.location = "index.php";
	}

	_this.onSocketConnection = function()
	{
		console.log("Connected to server.");
		_this.socket.emit("new client", {id: _this.userID, inGame: true});
		//_this.socket.emit("new player", _this.currentPlayer.getInfo());
	}

	_this.onPlayerDisconnect = function(data)
	{
		var player = _this.findPlayer(data.id);
		_this._webGLManager.scene.remove(player.mesh);
		var pIndex = _this.remotePlayers.indexOf(player);
		_this.remotePlayers.splice(pIndex);
	}

	_this.onNewPlayer = function(data)
	{
		_this.currentPlayer = _this.createPlayer(0,0, data.teamColor);
		_this.controllingPlayer = _this.currentPlayer;
		_this.currentPlayer.pID = data.id;
		console.log("Set ID: " + data.id);

		_this.playerSelector = loadModel("main/selector.js", new THREE.MeshLambertMaterial({color: data.teamColor}));
		_this.playerSelector.position.set(0,0,20);
		_this._webGLManager.addToScene(_this.playerSelector);

		_this.currentGameMode = data.gameType;

	}

	_this.findPlayer = function(id)
	{
		for (var i=0; i < _this.remotePlayers.length; i++)
		{
			if (_this.remotePlayers[i].pID == id)
				return _this.remotePlayers[i];
		}
	}

	_this.onServerUpdate = function(data)
	{
		if (_this.controllingPlayer != null)
		{
			if (_this.currentPlayer.pID != undefined)
			{
				var playerPositions = data[0];
				for (var i = 0; i < playerPositions.length; i ++)
				{
					var playerPos = playerPositions[i];
					if (playerPos.id == _this.currentPlayer.pID)
						_this.currentPlayer.setPosition(playerPos.x, playerPos.y);
					else
					{
						var player = _this.findPlayer(playerPos.id);
						if (player != null)
						{
							player.setPosition(playerPos.x, playerPos.y);
							if (player != _this.secondPlayer)
							{
								player.setRotation(playerPos.rotation);
								player.setMouse(playerPos.mouseX, playerPos.mouseY);
							}
						} else {
							var newPlayer = _this.createPlayer(playerPos.x, playerPos.y, playerPos.teamColor);
							newPlayer.pID = playerPos.id;
							if (newPlayer.pID.replace("s","") == _this.currentPlayer.pID)
								_this.secondPlayer = newPlayer;
							newPlayer.setRotation(playerPos.rotation);
							_this.remotePlayers.push(newPlayer);
						}
					}
				}
			} else {
				console.log("Undefined pID");
			}

			var ballData = data[1];
			if (ballData.controlledBy == _this.currentPlayer.pID)
			{
				if (_this.gameBall.controlledBy != _this.currentPlayer)
				{
					_this.gameBall.setControlledBy(_this.currentPlayer);
					if (_this.secondPlayer != null)
					{
						_this.controllingPlayer = _this.secondPlayer;
						_this.socket.emit("input", {uID: _this.currentPlayer.pID, keyboard: {}});
					}
				}
			}
			else if (_this.secondPlayer != null && ballData.controlledBy == _this.secondPlayer.pID)
			{
				if (_this.gameBall.controlledBy != _this.secondPlayer)
				{
					_this.gameBall.setControlledBy(_this.secondPlayer);
					_this.controllingPlayer = _this.currentPlayer;
					_this.socket.emit("input", {uID: _this.secondPlayer.pID, keyboard: {}});
				}
			}
			else
			{
				if (ballData.controlledBy != "")
				{
					var rPlayer = _this.findPlayer(ballData.controlledBy);
					if (rPlayer != null && _this.gameBall.controlledBy == null)
						_this.gameBall.setControlledBy(rPlayer);
					if (rPlayer != null)
						_this.gameBall.updateArrow(rPlayer.mousePos);
				} else {
					_this.gameBall.shoot();
				}
			}
			_this.gameBall.setBallData(ballData.x, ballData.y, ballData.velocityX, ballData.velocityY);

			var timeLeft = data[2];
			var seconds = timeLeft / 1000;
			var minutes = Math.floor(seconds / 60);
			var secondsLeft = Math.round(seconds - (minutes * 60));

			$("#time").text(minutes + ":" + ("0" + secondsLeft).slice(-2));
			$("#redScore").text(data[3]);
			$("#blueScore").text(data[4]);
		}
	}

	_this.createGame = function()
	{
		_this.createArena();

		_this.gameBall = new Ball();
		_this.gameBall.init();

		_this._webGLManager.addToScene(_this.gameBall.mesh);
	}

	var spacebarPressed = false;

	_this.update = function(deltaTime)
	{
		if (_this.controllingPlayer != null)
		{
			var mousePos = _this._mouseManager.getMouse3D(_this._webGLManager.camera);

			_this.gameBall.update(deltaTime * 50);

			if (_this.gameBall.controlledBy == _this.currentPlayer || (_this.secondPlayer != null && _this.gameBall.controlledBy == _this.secondPlayer))
				_this.gameBall.updateArrow(mousePos);

			if (_this._mouseManager.leftClickDown == true)
			{
				if (_this.gameBall.controlledBy == _this.currentPlayer)
				{
					_this.gameBall.shoot();
					_this.socket.emit("shoot", {uID: _this.currentPlayer.pID, mouseX: mousePos.x, mouseY: mousePos.y});
				}
				if (_this.gameBall.controlledBy == _this.secondPlayer)
				{
					_this.gameBall.shoot();
					_this.socket.emit("shoot", {uID: _this.secondPlayer.pID, mouseX: mousePos.x, mouseY: mousePos.y});
				}
			}

			if (_this._keyboardManager.isKeyDown(32))
			{
				if (!spacebarPressed)
				{
					if (_this.controllingPlayer == _this.currentPlayer)
					{
						if (_this.gameBall.controlledBy != _this.secondPlayer)
						{
							_this.controllingPlayer = _this.secondPlayer;
							_this.socket.emit("input", {uID: _this.currentPlayer.pID, keyboard: {}});
						}
					} else {
						if (_this.gameBall.controlledBy != _this.currentPlayer)
						{
							_this.controllingPlayer = _this.currentPlayer;
							_this.socket.emit("input", {uID: _this.secondPlayer.pID, keyboard: {}});
						}
					}
					spacebarPressed = true;
				}
			} else {
				spacebarPressed = false;
			}

			_this._webGLManager.followMesh = _this.controllingPlayer.mesh;

			if (_this.gameBall.controlledBy != _this.controllingPlayer)
			{
				if (_this._keyboardManager.isKeyDown(65) && _this.controllingPlayer.mesh.position.x > -FIELD_WIDTH / 2 + 15)
					_this.controllingPlayer.move(-75 * deltaTime,0);
				if (_this._keyboardManager.isKeyDown(87) && _this.controllingPlayer.mesh.position.y < FIELD_HEIGHT / 2 - 15)
					_this.controllingPlayer.move(0,75 * deltaTime);
				if (_this._keyboardManager.isKeyDown(68) && _this.controllingPlayer.mesh.position.x < FIELD_WIDTH / 2 - 15)
					_this.controllingPlayer.move(75 * deltaTime,0);
				if (_this._keyboardManager.isKeyDown(83) && _this.controllingPlayer.mesh.position.y > -FIELD_HEIGHT / 2 + 15)
					_this.controllingPlayer.move(0,-75 * deltaTime);
			}

			_this.playerSelector.position.set(_this.controllingPlayer.mesh.position.x, _this.controllingPlayer.mesh.position.y, 5);
			//_this.playerSelector.rotation.x += 0.05;

			_this.currentPlayer.lookat(mousePos);
			_this.socket.emit("update player", {uID: _this.currentPlayer.pID, rotation: _this.currentPlayer.mesh.rotation.z, mouseX: mousePos.x, mouseY: mousePos.y});
			_this.socket.emit("input", {uID: _this.controllingPlayer.pID, keyboard: _this._keyboardManager.pressedKeys});

			if (_this.secondPlayer != null)
			{
				_this.secondPlayer.lookat(mousePos);
				_this.socket.emit("update player", {uID: _this.secondPlayer.pID, rotation: _this.secondPlayer.mesh.rotation.z, mouseX: mousePos.x, mouseY: mousePos.y});
			}

			_this._webGLManager.zoom += _this._mouseManager.mouseScroll / 2000.0;
			_this._mouseManager.mouseScroll = 0;
		}
	}

		



	_this.createPlayer = function(x,y,teamColor)
	{
		var newPlayer = new Player();
		newPlayer.init(x,y,teamColor);
		_this._webGLManager.addToScene(newPlayer.mesh);

		return newPlayer;
	}

	_this.createArena = function()
	{
		var arenaGroup = new THREE.Object3D();
		// var plane = new THREE.Mesh(new THREE.PlaneGeometry(FIELD_WIDTH,FIELD_HEIGHT), new THREE.MeshLambertMaterial({color: 0xF9F9F9}));
		// //plane.receiveShadow = true;
		// arenaGroup.add(plane);

		// var topWall = new THREE.Mesh(new THREE.CubeGeometry(FIELD_WIDTH,10,50), new THREE.MeshLambertMaterial({color: 0x707070}));
		// topWall.position.set(0,FIELD_HEIGHT / 2,5);
		// arenaGroup.add(topWall);

		// var bottomWall = new THREE.Mesh(new THREE.CubeGeometry(FIELD_WIDTH,10,50), new THREE.MeshLambertMaterial({color: 0x707070}));
		// bottomWall.position.set(0,-FIELD_HEIGHT / 2,5);
		// arenaGroup.add(bottomWall);

		// var leftWall = new THREE.Mesh(new THREE.CubeGeometry(10,FIELD_HEIGHT - 10,50), new THREE.MeshLambertMaterial({color: 0x707070}));
		// leftWall.position.set(-FIELD_WIDTH / 2 + 5,0,5);
		// arenaGroup.add(leftWall);

		// var rightWall = new THREE.Mesh(new THREE.CubeGeometry(10,FIELD_HEIGHT - 10,50), new THREE.MeshLambertMaterial({color: 0x707070}));
		// rightWall.position.set(FIELD_WIDTH / 2 - 5,0,5);
		// arenaGroup.add(rightWall);

		var arena = loadUVModel("arenas/snow/snow.js", "arenas/snow/snow_textures.png");
		//var arena = loadModel("arenas/snow/snow.js", new THREE.MeshLambertMaterial({color: 0x2F2FFF}));
		arena.scale.set(10,10,10);
		arena.position.set(0,0,0);

		arenaGroup.add(arena);

		var topLeftGoalPost = new THREE.Mesh(new THREE.CubeGeometry(3,3,50), new THREE.MeshLambertMaterial({color: 0x2F2FFF}));
		topLeftGoalPost.position.set(-(FIELD_WIDTH / 2) + GOAL_DISTANCE, (GOAL_SIZE / 2), 5);
		arenaGroup.add(topLeftGoalPost);

		var bottomLeftGoalPost = new THREE.Mesh(new THREE.CubeGeometry(3,3,50), new THREE.MeshLambertMaterial({color: 0x2F2FFF}));
		bottomLeftGoalPost.position.set(-(FIELD_WIDTH / 2) + GOAL_DISTANCE, -(GOAL_SIZE / 2), 5);
		arenaGroup.add(bottomLeftGoalPost);

		var topRightGoalPost = new THREE.Mesh(new THREE.CubeGeometry(3,3,50), new THREE.MeshLambertMaterial({color: 0xFF0F0F}));
		topRightGoalPost.position.set((FIELD_WIDTH / 2) - GOAL_DISTANCE, (GOAL_SIZE / 2), 5);
		arenaGroup.add(topRightGoalPost);

		var bottomRightGoalPost = new THREE.Mesh(new THREE.CubeGeometry(3,3,50), new THREE.MeshLambertMaterial({color: 0xFF0F0F}));
		bottomRightGoalPost.position.set((FIELD_WIDTH / 2) - GOAL_DISTANCE, -(GOAL_SIZE / 2), 5);
		arenaGroup.add(bottomRightGoalPost);

		_this._webGLManager.addToScene(arenaGroup);
	}

}

/*
Keyboard Manager
*/
function KeyboardManager()
{
	var _this = this;
	this.pressedKeys = {};

	this.init = function()
	{
		window.addEventListener("keydown", _this.onKeyDown, false);
		window.addEventListener("keyup", _this.onKeyUp, false);
	}

	this.onKeyDown = function(event)
	{
		_this.pressedKeys[event.which] = true;
	}

	this.onKeyUp = function(event)
	{
		_this.pressedKeys[event.which] = false;
	}

	this.isKeyDown = function(keyCode)
	{
		if (keyCode in this.pressedKeys)
			return this.pressedKeys[keyCode];
		else
			return false;
	}
}

/*
Mouse Manager
*/
function MouseManager(container)
{
	var _this = this;
	this.mouseX = 0;
	this.mouseY = 0;
	this.mouseScroll = 0;
	this.mouse3D = null;
	this.container = container;
	this.projector = null;
	this.leftClickDown = false;

	this.init = function()
	{
		window.addEventListener("mousemove", _this.onMouseMove, false)
		window.addEventListener("DOMMouseScroll", _this.onMouseScroll, false);
		window.addEventListener("mousewheel", _this.onMouseScroll, false);
		window.addEventListener("mousedown", _this.onMouseDown, false);
		window.addEventListener("mouseup", _this.onMouseUp, false);
		this.projector = new THREE.Projector();
	}

	this.onMouseDown = function(event)
	{
		if (event.button == 0)
			_this.leftClickDown = true;
	}

	this.onMouseUp = function(event)
	{
		if (event.button == 0)
			_this.leftClickDown = false;
	}

	this.onMouseScroll = function(event)
	{
		_this.mouseScroll = event.wheelDelta;
	}

	this.onMouseMove = function(event)
	{
		// _this.mouseX = event.clientX - $(this.container).position().left;
		// _this.mouseY = event.clientY - $(this.container).position().top;
		_this.mouseX = event.clientX;
		_this.mouseY = event.clientY;
	}

	this.getMouse3D = function(camera)
	{
		var vector = new THREE.Vector3((_this.mouseX / window.innerWidth) * 2 - 1, -(_this.mouseY / window.innerHeight) * 2 + 1, 0.5);
		this.projector.unprojectVector(vector, camera);
		var dir = new THREE.Vector3(vector.x - camera.position.x, vector.y - camera.position.y, vector.z - camera.position.z).normalize();
		var distance = -camera.position.z / dir.z;
		this.mouse3D = camera.position.clone().add( dir.multiplyScalar(distance));

		return this.mouse3D;
	}	

}

function loadModel(_model, meshMat, modelFix, skinned) {
	modelFix = modelFix || false;
	skinned = skinned || false;
    var group = new THREE.Object3D();
    // var material = meshMat || new THREE.MeshBasicMaterial({
    //     color: '0x' + Math.floor(Math.random() * 16777215).toString(16),
    //     wireframe: true
    // });
    var loader = new THREE.JSONLoader();
    loader.load("ultimatefrisbee/models/" + _model,
        function(geometry, materials) {

        	if(skinned)
        	{
        		//THREE.AnimationHandler.add(geometry.animations);
        		for (var k in materials) {
			        materials[k].skinning = true;
			    }
        	}

        	var material = meshMat || new THREE.MeshFaceMaterial(materials);
        	material.side = THREE.FrontSide;
        	if (!skinned)
            	var mesh = new THREE.Mesh(geometry, material);
            else
            {
            	var mesh = new THREE.SkinnedMesh(geometry, material);
    
            }
            mesh.position.x = mesh.position.y = mesh.position.z = 0;
            mesh.rotation.y = mesh.rotation.z = 0;
            if (modelFix)
            {
	            mesh.rotation.x = Math.PI / 2;
	            mesh.rotation.y = Math.PI;
	            mesh.scale.set(3,3,3);
	        }
            mesh.matrixAutoUpdate = false;
            mesh.updateMatrix();
            group.add(mesh);
            if (skinned)
            {
            	//console.log(mesh.geometry.animations[1]);
            	THREE.AnimationHandler.add(mesh.geometry.animations[1]);
            	animation = new THREE.Animation(mesh, "Baile", THREE.AnimationHandler.CATMULLROM);
            	animation.play();
            }
        }
    );

    return group;
}

function loadUVModel(_model, image) {
    var group = new THREE.Object3D();
    var material = new THREE.MeshBasicMaterial({map: THREE.ImageUtils.loadTexture("ultimatefrisbee/models/" + image)});
    var loader = new THREE.JSONLoader();
    loader.load("ultimatefrisbee/models/" + _model,
        function(geometry, materials) {
        	material.side = THREE.FrontSide;
            mesh = new THREE.Mesh(geometry, material);
            mesh.position.x = mesh.position.y = mesh.position.z = 0;
            mesh.rotation.x = mesh.rotation.y = mesh.rotation.z = 0;
            mesh.matrixAutoUpdate = false;
            mesh.updateMatrix();
            group.add(mesh);
        }
    );

    return group;
}

function log(data)
{
	$("#console").html(data);
}