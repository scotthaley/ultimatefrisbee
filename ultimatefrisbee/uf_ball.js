/*
Ball Class
*/

function Ball()
{
	this.controlledBy = null;
	this.velocityX = 0;
	this.velocityY = 0;
	this.lastControlledBy = null;
	this.lastControlledByCount = 0;


	this.init = function()
	{
		this.mesh = new THREE.Object3D();
		this.ballMesh = new THREE.Mesh(new THREE.SphereGeometry(5,16,16), new THREE.MeshLambertMaterial({color: 0x585A4E}));
		this.mesh.add(this.ballMesh);
		this.mesh.position.set(0,50,20);

		var geom = new THREE.Geometry();
		// Bottom of mesh
		geom.vertices.push(new THREE.Vector3(0,0,0));
		geom.vertices.push(new THREE.Vector3(10,15,0));
		geom.vertices.push(new THREE.Vector3(20,0,0));
		geom.vertices.push(new THREE.Vector3(17,0,0));
		geom.vertices.push(new THREE.Vector3(10,11,0));
		geom.vertices.push(new THREE.Vector3(3,0,0));
		// Top of mesh
		geom.vertices.push(new THREE.Vector3(0,0,3));
		geom.vertices.push(new THREE.Vector3(10,15,3));
		geom.vertices.push(new THREE.Vector3(20,0,3));
		geom.vertices.push(new THREE.Vector3(17,0,3));
		geom.vertices.push(new THREE.Vector3(10,11,3));
		geom.vertices.push(new THREE.Vector3(3,0,3));
		// Bottom Faces
		geom.faces.push(new THREE.Face3(0,4,1));
		geom.faces.push(new THREE.Face3(0,5,4));
		geom.faces.push(new THREE.Face3(1,4,2));
		geom.faces.push(new THREE.Face3(2,4,3));
		// Top Faces
		geom.faces.push(new THREE.Face3(6,10,7));
		geom.faces.push(new THREE.Face3(6,11,10));
		geom.faces.push(new THREE.Face3(7,10,8));
		geom.faces.push(new THREE.Face3(8,10,9));
		// Left Back Face
		geom.faces.push(new THREE.Face3(0,5,6));
		geom.faces.push(new THREE.Face3(5,11,6));
		// Right Back Face
		geom.faces.push(new THREE.Face3(3,2,8));
		geom.faces.push(new THREE.Face3(9,3,8));
		// Left Outside Face
		geom.faces.push(new THREE.Face3(1,0,6));
		geom.faces.push(new THREE.Face3(1,6,7));
		// Right Outside Face
		geom.faces.push(new THREE.Face3(2,1,8));
		geom.faces.push(new THREE.Face3(1,7,8));
		// Right Inside Face
		geom.faces.push(new THREE.Face3(4,3,9));
		geom.faces.push(new THREE.Face3(9,10,4));
		// Left Inside Face
		geom.faces.push(new THREE.Face3(5,4,10));
		geom.faces.push(new THREE.Face3(5,10,11));

		geom.computeFaceNormals();

		this.arrowHead = new THREE.Mesh(geom,  new THREE.MeshLambertMaterial({color: 0x585A4E}));
	}

	this.setBallData = function(x,y,vX,vY)
	{
		if (this.velocityX != vX)
			this.mesh.position.x = x;
		if (this.velocityY != vY)
			this.mesh.position.y = y;
		this.velocityX = vX;
		this.velocityY = vY;
	}

	this.updateArrow = function(mousePos)
	{
		var arrowDistance = new THREE.Vector3(this.mesh.position.x, this.mesh.position.y, 0).distanceTo(mousePos);
		if (arrowDistance < 50)
			arrowDistance = 50;
		if (arrowDistance > 200)
			arrowDistance = 200;
		var newArrowBody = new THREE.Mesh(new THREE.CubeGeometry(5,arrowDistance - 30,3), new THREE.MeshLambertMaterial({color: this.controlledBy.teamColor}));
		newArrowBody.position.set(0, (arrowDistance - 30) / 2 + 10, 5);
		this.mesh.remove(this.arrowBody);
		this.arrowBody = newArrowBody;
		this.mesh.add(this.arrowBody);

		this.arrowHead.position.set(-10, arrowDistance - 22, 3);
	}

	this.update = function(delta)
	{
		if (this.lastControlledByCount > 0)
			this.lastControlledByCount --;
		else
			this.lastControlledBy = null;

		if (this.controlledBy != null)
		{
			this.mesh.position.set(this.controlledBy.mesh.position.x, this.controlledBy.mesh.position.y, 20);
			this.mesh.rotation.z = this.controlledBy.mesh.rotation.z;
		} else {
			var newX = this.mesh.position.x + this.velocityX * delta;
			var newY = this.mesh.position.y + this.velocityY * delta;

			this.mesh.position.set(newX, newY, this.mesh.position.z);
		}
	}

	this.setControlledBy = function(player)
	{
		if (player != this.lastControlledBy)
		{
			this.controlledBy = player;
			this.ballMesh.material.color.setHex(this.controlledBy.teamColor);

			this.arrowBody = new THREE.Mesh(new THREE.CubeGeometry(5,20,3), new THREE.MeshLambertMaterial({color: this.controlledBy.teamColor}));
			this.arrowBody.position.set(0, 20, -5);
			this.arrowHead.material.color.setHex(this.controlledBy.teamColor);
			this.mesh.add(this.arrowBody);
			this.mesh.add(this.arrowHead);
		}
	}

	this.shoot = function()
	{
		this.controlledBy = null;
		this.mesh.remove(this.arrowBody);
		this.mesh.remove(this.arrowHead);
	}
}