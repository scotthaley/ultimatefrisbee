/*
three.js Manager Class
*/

function webGLManager()
{
	this.followMesh = null;
	this.zoom = 1;
	this.updateCallback = null;
	this.camera = null;

	this.init = function(container)
	{
		this.scene = new THREE.Scene();
		// this.WIDTH = $(container).width()
		// this.HEIGHT = $(container).height()
		this.WIDTH = window.innerWidth;
		this.HEIGHT = window.innerHeight;
		this.renderer = new THREE.WebGLRenderer({ antialias: true});
		this.renderer.setSize(this.WIDTH,this.HEIGHT);
		document.body.appendChild(this.renderer.domElement);

		this.camera = new THREE.PerspectiveCamera(45, this.WIDTH / this.HEIGHT, 0.1, 20000);
		this.camera.position.set(0,-80,300);
		this.camera.rotation.x = 0.75;
		this.scene.add(this.camera);

		var parameters = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter,format: THREE.RGBFormat, stencilBuffer: false };
		var renderTarget = new THREE.WebGLRenderTarget( this.WIDTH, this.HEIGHT, parameters );

		this.composer = new THREE.EffectComposer( this.renderer, renderTarget ); 
		this.composer.addPass(new THREE.RenderPass( this.scene, this.camera));

		var fxaa = new THREE.ShaderPass(THREE.FXAAShader);
		fxaa.uniforms['resolution'].value = new THREE.Vector2(1/this.WIDTH, 1/this.HEIGHT);
		this.composer.addPass(fxaa);

		var effect = new THREE.ShaderPass(THREE.CopyShader);
		effect.renderToScreen = true;
		this.composer.addPass(effect);

		this.renderer.shadowMapEnabled = true;
		this.renderer.shadowMapSoft = true;
		this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
		this.renderer.shadowCameraNear = 3;
		this.renderer.shadowCameraFar = this.camera.far;
		this.renderer.shadowCameraFov = 75;
		this.renderer.shadowMapBias = 0.0039;
		this.renderer.shadowMapDarkness = 0.5;
		this.renderer.shadowMapWidth = 1024;
		this.renderer.shadowMapHeight = 1024;
		this.renderer.physicallyBasedShading = true;

		var spotlight1 = new THREE.SpotLight(0xffffff, 9, 400, Math.PI/2, 1);
		spotlight1.position.set(-FIELD_WIDTH / 2,0, 300);
		spotlight1.target.position.set(0,0,0);
		//spotlight1.castShadow = true;
		spotlight1.shadowCameraVisible = false;
		spotlight1.shadowDarkness = 0.5;
		this.scene.add(spotlight1);

		var spotlight2 = new THREE.SpotLight(0xffffff, 9, 400, Math.PI/2, 1);
		spotlight2.position.set(FIELD_WIDTH / 2,0, 300);
		spotlight2.target.position.set(0,0,0);
		//spotlight2.castShadow = true;
		spotlight2.shadowCameraVisible = false;
		spotlight2.shadowDarkness = 0.5;
		this.scene.add(spotlight2);

		var spotlight3 = new THREE.SpotLight(0xffffff, 9, 400, Math.PI/2, 1);
		spotlight3.position.set(0,0, 350);
		spotlight3.target.position.set(0,0,0);
		//spotlight3.castShadow = true;
		spotlight3.shadowCameraVisible = false;
		spotlight3.shadowDarkness = 0.5;
		this.scene.add(spotlight3);

	    // var directionalLight = new THREE.DirectionalLight(0xffffff, 1.5);
	    // directionalLight.position.set(100,-200,200);
	    // directionalLight.castShadow = true;
	    // this.scene.add(directionalLight);

		this.update();
	}

	this.registerUpdate = function(callback)
	{
		this.updateCallback = callback;
	}

	this.addToScene = function(objectToAdd)
	{
		this.scene.add(objectToAdd);
	}

	var then;

	this.update = function()
	{
		var now = Date.now();
		var delta = (now - then) / 1000;

		if (this.updateCallback != null)
			this.updateCallback(delta);

		then = now;

		if (this.followMesh != null)
			this.camera.position.set(this.followMesh.position.x, this.followMesh.position.y - 120 / this.zoom, 150 / this.zoom);

		this.renderer.render(this.scene, this.camera);
		this.composer.render();

		THREE.AnimationHandler.update( delta );

		var _this = this;
		requestAnimationFrame(function() {
			_this.update();
		});
	}
}