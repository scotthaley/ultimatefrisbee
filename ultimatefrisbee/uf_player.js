/*
Player Class
*/

function Player()
{
	this.pID = "x";

	this.init = function(x,y,teamColor)
	{
		teamColor = teamColor || 0x2F2FFF;
		this.mesh = new THREE.Object3D();
		//this.mesh.add(new THREE.Mesh(new THREE.CubeGeometry(5,5,20), new THREE.MeshLambertMaterial({color: teamColor})));
		if (teamColor == 0x2F2FFF)
			this.mesh.add(loadModel("skins/MountainManBlue.js", null, true, false));
		else
			this.mesh.add(loadModel("skins/MountainManRed.js", null, true, false));
		this.mesh.position.set(x,y,20);
		this.teamColor = teamColor;
	}

	this.setMouse = function(x,y)
	{
		this.mousePos = new THREE.Vector3(x,y,0);
	}

	this.setPosition = function(x,y)
	{
		this.mesh.position.set(x, y, this.mesh.position.z);
	}

	this.move = function(x,y)
	{
		this.mesh.position.set(this.mesh.position.x + x, this.mesh.position.y + y, this.mesh.position.z);
	}

	this.lookat = function(vector)
	{
		var p1 = {x: vector.x, y: vector.y};
		var p2 = {x: this.mesh.position.x, y: this.mesh.position.y};
		var angle = Math.atan2(p2.y - p1.y, p2.x - p1.x);
		this.mesh.rotation.z = angle + Math.PI / 2;
	}

	this.setRotation = function(angle)
	{
		this.mesh.rotation.z = angle;
	}

	this.getInfo = function()
	{
		return {x: this.mesh.position.x, y: this.mesh.position.y, rotation: this.mesh.rotation.z};
	}
}