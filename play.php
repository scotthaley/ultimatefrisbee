<?php
session_start();
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Ultimate Frisbee | In Game</title>
		<style>
		body{
			margin: 0px;
			overflow: hidden;
		}
		#time{
			position:fixed;
			width:300px;
			height:100px;
			top:50px;
			left:50%;
			margin-left: -150px;
			color: #747766;
			font-size:70px;
			text-align: center;
		}
		#blueScore{
			position:fixed;
			width:300px;
			height:100px;
			top:60px;
			left:50%;
			margin-left: -350px;
			color: #0016DD;
			font-size:50px;
			text-align: center;
		}
		#redScore{
			position:fixed;
			width:300px;
			height:100px;
			top:60px;
			left:50%;
			margin-left: +50px;
			color: #DD0000;
			font-size:50px;
			text-align: center;
		}
		</style>
	</head>
	<body>

		<div id="time">2:00</div>
		<div id="blueScore">0</div>
		<div id="redScore">0</div>
		
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script src="ultimatefrisbee/three/three.js"></script>
	<script src="ultimatefrisbee/three/EffectComposer.js"></script>
	<script src="ultimatefrisbee/three/FXAAShader.js"></script>
	<script src="ultimatefrisbee/three/CopyShader.js"></script>
	<script src="ultimatefrisbee/three/ShaderPass.js"></script>
	<script src="ultimatefrisbee/three/MaskPass.js"></script>
	<script src="ultimatefrisbee/three/RenderPass.js"></script>

	<script src="ultimatefrisbee/uf_ball.js"></script>
	<script src="ultimatefrisbee/uf_player.js"></script>
	<script src="ultimatefrisbee/uf_webglmanager.js"></script>
	<script src="ultimatefrisbee/ultimatefrisbee.js"></script>
	<script src="http://99.136.94.105:8000/socket.io/socket.io.js"></script>

	<script type="text/javascript">

	var newGame = new ultimatefrisbee();
	newGame.init("#container", <?php echo $_SESSION["id"] ?>);

	</script>

</html>

