var util = require("util");
var io = require("socket.io");

var socket, connectedClients, allGames;

init();

function init()
{
	connectedClients = [];
	allGames = [];

	socket = io.listen(8000);
	socket.configure(function () {
		socket.set("transports", ["websocket"]);
		socket.set("log level", 2);
	});
	socket.sockets.on("connection", onSocketConnection);

	setInterval(updateClients, 1000);
	setInterval(mainServerLoop, 16.67);
}

function mainServerLoop()
{
	for (var i = 0; i < allGames.length; i ++)
	{
		allGames[i].mainLoop(socket);
	}
}

function updateClients()
{
	var clientsToRemove = [];

	for (var i = 0; i < connectedClients.length; i++)
	{
		var client = connectedClients[i];
		if (!client.isConnected)
		{
			if (client.timeout == 0)
			{
				clientsToRemove.push(client);
			} else {
				client.timeout --;
			}
		}
	}

	for (var i = 0; i < clientsToRemove.length; i++)
	{
		var client = clientsToRemove[i];
		var index = connectedClients.indexOf(client);
		connectedClients.splice(index, 1);
	}

	var gamesToRemove = [];

	for (var i = 0; i < allGames.length; i ++)
	{
		if (!allGames[i].updateLobby(socket))
		{
			gamesToRemove.push(allGames[i]);
		}

		if (allGames[i].gameStarted == false)
		{
			allGames[i].updateSearchingClients(socket);
		}
	}

	for (var i = 0; i < gamesToRemove.length; i ++)
	{
		var game = gamesToRemove[i];
		var index = allGames.indexOf(game);
		util.log("Removed game: " + game.gameLobby);
		allGames.splice(index, 1);
	}

	socket.sockets.in("dashboard").emit("update client", {count: connectedClients.length});
}

function onSocketConnection(client) {
	util.log("New player has connected: "+client.id);
	client.on("disconnect", onClientDisconnect);
	client.on("new client", onNewClient);
	client.on("query players", onQueryPlayers);
	client.on("search game", onSearchGame);
	client.on("accept game", onAcceptGame);
	client.on("update player", onUpdatePlayer);
	client.on("input", onInput);
	client.on("shoot", onShoot);
}

function onQueryPlayers(data)
{
	var playerData = {};
	for (var i = 0; i < data.length; i ++)
	{
		if (findClientByUserID(data[i]) != null)
			playerData[data[i]] = true;
		else
			playerData[data[i]] = false;
	}
	this.emit("player status", playerData);
}

function onClientDisconnect()
{
	util.log("Player has disconnected: "+this.id);
	var client = findClientByNodeID(this.id);
	if (client != null)
	{
		if (client.isConnected)
		{
			client.isConnected = false;
			client.timeout = 10;
		}
	}
}

function onNewClient(data)
{
	var client = findClientByUserID(data.id)
	if (client == null)
	{
		var newClient = new Client();
		newClient.init(data.id, this.id);
		connectedClients.push(newClient);
		this.join("dashboard");
		if (data.inGame)
			this.emit("not in game");
	} else {
		client.nodeID = this.id;
		client.isConnected = true;
		if (client.currentGame == null)
		{
			if (data.inGame)
				this.emit("not in game");
			this.join("dashboard");
		}
		else
		{
			this.join(client.currentGame.gameLobby);
			this.emit("new player", {id:client.userID, gameType: client.currentGame.gameType, teamColor: findPlayerByClient(client).teamColor});
		}
	}
}

function onSearchGame(data)
{
	if (findPlayerByClient(findClientByNodeID(this.id)) != null)
		return;

	var joinGame = null;
	if (allGames.length != 0)
	{
		for (var i = 0; i < allGames.length; i ++)
		{
			var game = allGames[i];
			if (!game.gameFull)
			{
				joinGame = game;
			}
		}
	}

	if (joinGame == null)
	{
		var newGame = new FrisbeeGame();
		newGame.init(data.gameType);

		allGames.push(newGame);
		var gameNumb = allGames.length - 1;

		util.log("Created game: game-" + gameNumb);

		newGame.gameLobby = "game-" + gameNumb;

		newGame.addPlayer(findClientByNodeID(this.id));
		this.join("game-" + gameNumb);
	} else {
		joinGame.addPlayer(findClientByNodeID(this.id));
		this.join(joinGame.gameLobby);
	}
}

function onAcceptGame()
{
	var client = findClientByNodeID(this.id);
	var player = findPlayerByClient(client);
	if (player != null)
		player.accepted = true;

	var secondPlayer = findPlayerByClient(client, true);
	if (secondPlayer != null)
		secondPlayer.accepted = true;
}

function onUpdatePlayer(data) {
	var client = findClientByNodeID(this.id);
	if (data.uID.indexOf("s") == -1)
		var player = findPlayerByClient(client);
	else
		var player = findPlayerByClient(client, true);
	if (player != null)
		player.updatePlayer(data.rotation, data.mouseX, data.mouseY);
}

function onInput(data)
{
	var client = findClientByNodeID(this.id);
	if (data.uID.indexOf("s") == -1)
		var player = findPlayerByClient(client);
	else
		var player = findPlayerByClient(client, true);
	if (player != null)
		player.pressedKeys = data.keyboard;
}

function onShoot(data)
{
	var client = findClientByNodeID(this.id);
	if (data.uID.indexOf("s") == -1)
		var player = findPlayerByClient(client);
	else
		var player = findPlayerByClient(client, true);
	var game = client.currentGame;

	if (player == game.gameBall.controlledBy)
	{
		game.gameBall.lastControlledBy = game.gameBall.controlledBy;
		game.gameBall.lastControlledByCount = 15;
		game.gameBall.controlledBy = null;
		// gameBall.velocityX = (data.mouseX - gameBall.positionX) / 50;
		// gameBall.velocityY = (data.mouseY - gameBall.positionY) / 50;

		var p1 = {x: data.mouseX, y: data.mouseY};
		var p2 = {x: game.gameBall.positionX, y: game.gameBall.positionY};
		var angle = Math.atan2(p2.y - p1.y, p2.x - p1.x) + Math.PI;
		var distance = Math.sqrt(Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y));
		if (distance > 15)
			distance = 15;
		if (distance < 6)
			distance = 6;
		game.gameBall.velocityX = Math.cos(angle) * Math.pow(distance / 7, 2);
		game.gameBall.velocityY = Math.sin(angle) * Math.pow(distance / 7, 2);
	}
}

// Find Clients
function findClientByNodeID(id)
{
	for (var i = 0; i < connectedClients.length; i ++)
	{
		if (connectedClients[i].nodeID == id)
			return connectedClients[i];
	}
}

function findClientByUserID(id)
{
	for (var i = 0; i < connectedClients.length; i ++)
	{
		if (connectedClients[i].userID == id)
			return connectedClients[i];
	}
}

function findPlayerByClient(client, second)
{
	second = second || false;
	for (var i = 0; i < allGames.length; i ++)
	{
		if (!second)
			var player = allGames[i].getPlayer(client);
		else
			var player = allGames[i].getSecondPlayer(client);
		if (player != null)
			return player;
	}
}

// Client
function Client()
{
	this.init = function(userID, nodeID)
	{
		this.nodeID = nodeID;
		this.userID = userID.toString();
		this.isConnected = true;
		this.timeout = 0;
		this.currentGame = null;
	}
}

function FrisbeeGame()
{
	this.gameLobby = "";
	this.gameStarted = false;
	this.gameTimeout = 30;
	this.gameFull = false;
	this.totalGameTime = 600000;
	this.FIELD_WIDTH = 600;
	this.FIELD_HEIGHT = 300;
	this.GOAL_DISTANCE = 75;
	this.GOAL_SIZE = 40;
	this.GOAL_POST_SIZE = 3;

	var _this = this;

	this.init = function(gameType)
	{
		this.players = [];
		this.gameBall = new Ball();
		this.gameBall.setFieldSize(this.FIELD_WIDTH, this.FIELD_HEIGHT, this.GOAL_DISTANCE, this.GOAL_SIZE, this.GOAL_POST_SIZE);
		this.gameBall.velocityY = 2;
		this.gameType = gameType;
		this.gameBall.setScoreCallback(this.teamScore);

		if (gameType == 1)
		{
			this.requiredPlayers = 4;
			this.useSecondPlayer = true;
		}
	}

	this.teamScore = function(team)
	{
		if (team == "red")
			_this.redScore ++;
		else
			_this.blueScore ++;

		for (var i = 0; i < _this.players.length; i ++)
		{
			_this.players[i].resetPlayer();
		}

		_this.gameBall.velocityX = 0;
		_this.gameBall.velocityY = 2;
		_this.gameBall.positionX = 0;
		_this.gameBall.positionY = 0;
	}

	this.addPlayer = function(client)
	{
		if (client != null)
		{
			client.currentGame = this;
			var newPlayer = new Player();
			if (this.players.length < this.requiredPlayers / 2)
			{
				newPlayer.init(client,-100,50);
				newPlayer.teamColor = 0x2F2FFF;
			}
			else
			{
				newPlayer.init(client,100,50);
				newPlayer.teamColor = 0xFF0F0F;
			}
			this.players.push(newPlayer);
			util.log("Player joined: " + client.userID);

			if (this.useSecondPlayer)
			{
				var secondPlayer = new Player();
				secondPlayer.initSecond(client, newPlayer.x, -50);
				secondPlayer.teamColor = newPlayer.teamColor;
				this.players.push(secondPlayer);
				util.log("Player joined: " + client.userID + "s");
			}

			util.log(this.gameLobby + " has " + this.players.length + " players.");

			if (this.players.length == this.requiredPlayers)
				this.gameFull = true;
		}
	}

	this.getPlayer = function(client)
	{
		for (var i = 0; i < this.players.length; i ++)
		{
			if (this.players[i].client == client && !this.players[i].second)
				return this.players[i];
		}
	}

	this.getSecondPlayer = function(client)
	{
		for (var i = 0; i < this.players.length; i ++)
		{
			if (this.players[i].client == client && this.players[i].second)
				return this.players[i];
		}
	}

	this.updateSearchingClients = function(socket)
	{
		var playersLeft = this.requiredPlayers - this.players.length;
		if (this.useSecondPlayer)
			playersLeft = playersLeft / 2;
		socket.sockets.in(this.gameLobby).emit("searching", {waiting: playersLeft, gameType: this.gameType});
	}

	this.startGame = function(socket)
	{
		this.gameStarted = true;
		socket.sockets.in(this.gameLobby).emit("game started");

		this.gameStartTime = Date.now();
		this.blueScore = 0;
		this.redScore = 0;
	}

	var then = Date.now();

	this.mainLoop = function(socket)
	{
		var now = Date.now();
		var delta = (now - then) / 1000;
		var timeLeft = this.totalGameTime - (now - this.gameStartTime);

		this.gameBall.update(delta * 50);

		var gameData = [];
		var playerPositions = [];
		for (var i = 0; i < this.players.length; i ++)
		{
			if (this.gameBall.controlledBy != this.players[i])
			{
				if (65 in this.players[i].pressedKeys && this.players[i].pressedKeys[65] == true && this.players[i].x > -this.FIELD_WIDTH / 2 + 15)
					this.players[i].move(-75 * delta,0);
				if (87 in this.players[i].pressedKeys && this.players[i].pressedKeys[87] == true && this.players[i].y < this.FIELD_HEIGHT / 2 - 15)
					this.players[i].move(0,75 * delta);
				if (68 in this.players[i].pressedKeys && this.players[i].pressedKeys[68] == true && this.players[i].x < this.FIELD_WIDTH / 2 - 15)
					this.players[i].move(75 * delta,0);
				if (83 in this.players[i].pressedKeys && this.players[i].pressedKeys[83] == true && this.players[i].y > -this.FIELD_HEIGHT / 2 + 15)
					this.players[i].move(0,-75 * delta);
			}

			// if (this.players[i].second)
			// 	this.players[i].move(0,10 * delta);

			playerPositions.push(this.players[i].getInfo());

			//util.log(Math.pow(Math.abs(this.gameBall.positionX - this.players[i].x),2) + Math.pow(Math.abs(this.gameBall.positionY - this.players[i].y),2));

			if (Math.pow(Math.abs(this.gameBall.positionX - this.players[i].x),2) + Math.pow(Math.abs(this.gameBall.positionY - this.players[i].y),2) < 40 && this.gameBall.controlledBy == null && this.gameBall.lastControlledBy != this.players[i])
			{
				this.gameBall.controlledBy = this.players[i];
				this.gameBall.positionX = this.players[i].x;
				this.gameBall.positionY = this.players[i].y;
				this.gameBall.velocityX = 0;
				this.gameBall.velocityY = 0;
			}

		}
		gameData.push(playerPositions);

		var ballControlledBy = "";
		if (this.gameBall.controlledBy != null)
			ballControlledBy = this.gameBall.controlledBy.pID();
		var ballData = {x: this.gameBall.positionX, y: this.gameBall.positionY, controlledBy: ballControlledBy, velocityX: this.gameBall.velocityX, velocityY: this.gameBall.velocityY};
		gameData.push(ballData);

		gameData.push(timeLeft);

		gameData.push(this.blueScore);
		gameData.push(this.redScore);

		socket.sockets.in(this.gameLobby).emit("update", gameData);

		then = now;
	}

	this.updateLobby = function(socket)
	{
		if (this.players.length == this.requiredPlayers && !this.gameStarted)
		{
			var allAccepted = true;
			for (var i = 0; i < this.players.length; i ++)
			{
				if (this.players[i].accepted == false)
				{
					allAccepted = false;
					break;
				}
			}

			if (!allAccepted)
			{
				if (this.gameTimeout == 0)
				{
					socket.sockets.in(this.gameLobby).emit("found game", {timeout: this.gameTimeout});
					for(var i = 0; i < socket.sockets.clients(this.gameLobby).length; i ++)
					{
						socket.sockets.clients(this.gameLobby)[i].leave(this.gameLobby);
					}

					return false;
				}

				socket.sockets.in(this.gameLobby).emit("found game", {timeout: this.gameTimeout});
				this.gameTimeout --;
			} else {
				this.startGame(socket);
			}
		}

		return true;
	}
}

function Player()
{
	this.x = 0;
	this.y = 0;
	this.teamColor = 0;
	this.pressedKeys = [];

	this.init = function(client, x, y)
	{
		this.client = client;
		this.second = false;
		this.accepted = false;
		this.x = x;
		this.y = y;

		this.resetX = x;
		this.resetY = y;
	}

	this.initSecond = function(client, x, y)
	{
		this.client = client;
		this.second = true;
		this.accepted = false;
		this.x = x;
		this.y = y;
		this.rotation = 0;
		this.mouseX = 0;
		this.mouseY = 0;

		this.resetX = x;
		this.resetY = y;
	}

	this.resetPlayer = function()
	{
		this.x = this.resetX;
		this.y = this.resetY;
	}

	this.updatePlayer = function(rotation, mouseX, mouseY)
	{
		this.rotation = rotation;
		this.mouseX = mouseX;
		this.mouseY = mouseY;
	}

	this.move = function(x,y)
	{
		this.x = this.x + x;
		this.y = this.y + y;
	}

	this.pID = function()
	{
		var id = this.client.userID;
		if (this.second)
			id += "s";
		return id;
	}

	this.getInfo = function()
	{
		var id = this.client.userID;
		if (this.second)
			id += "s";
		return {id: this.pID(), x: this.x, y: this.y, rotation: this.rotation, mouseX: this.mouseX, mouseY: this.mouseY, teamColor: this.teamColor};
	}
}

function Ball()
{
	this.controlledBy = null;
	this.velocityX = 0;
	this.velocityY = 0;
	this.positionX = 0;
	this.positionY = 50;
	this.lastControlledBy = null;
	this.lastControlledByCount = 0;

	this.setFieldSize = function(width, height, goal_distance, goal_size, goal_post_size)
	{
		this.FIELD_WIDTH = width;
		this.FIELD_HEIGHT = height;
		this.GOAL_DISTANCE = goal_distance;
		this.GOAL_SIZE = goal_size;
		this.GOAL_POST_SIZE = goal_post_size;
	}

	this.setScoreCallback = function(callback)
	{
		this.scoreCallback = callback;
	}

	this.update = function(delta)
	{
		if (this.lastControlledByCount > 0)
			this.lastControlledByCount --;
		else
			this.lastControlledBy = null;

		var newX = this.positionX + this.velocityX * delta;
		var newY = this.positionY + this.velocityY * delta;
		if ((newX > this.FIELD_WIDTH / 2 - 13 && this.velocityX > 0) || (newX < -this.FIELD_WIDTH / 2 + 13 && this.velocityX < 0))
			this.velocityX = -this.velocityX;
		if ((newY > this.FIELD_HEIGHT / 2 - 10 && this.velocityY > 0) || (newY < -this.FIELD_HEIGHT / 2 + 10 && this.velocityY < 0))
			this.velocityY = -this.velocityY;


		// Goal Posts
		if (this.velocityX < 0)
		{
			// Left Side
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 5 && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + 2)
			{
				if (newY > this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 5 && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + 2)
			{
				if (newY > -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}

			// Score Left
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - 10)
			{
				if (newY > -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 && newY < this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2)
				{
					this.scoreCallback("blue");
					return;
				}
			}

			// Right Side
			if (newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 5 && newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + 2)
			{
				if (newY > this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}
			if (newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 5 && newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + 2)
			{
				if (newY > -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}

		} else {
			// Left Side
			if (newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 -5 && newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - 2)
			{
				if (newY > this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}
			if (newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 -5 && newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - 2)
			{
				if (newY > -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}

			// Right Side
			if (newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 -5 && newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - 2)
			{
				if (newY > this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}
			if (newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 -5 && newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - 2)
			{
				if (newY > -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 3 && newY < -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 3)
				{
					this.velocityX = -this.velocityX;
				}
			}

			// Score Left
			if (newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE && newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + 10)
			{
				if (newY > -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 && newY < this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2)
				{
					this.scoreCallback("red");
					return;
				}
			}
		}

		if (this.velocityY < 0)
		{
			// Left Side
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY < this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 5 && newY > this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY < -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 5 && newY > -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}

			// Right Side
			if (newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY < this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 5 && newY > this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}
			if (newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY < -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2 + 5 && newY > -this.GOAL_SIZE / 2 + this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}
		} else {
			// Left Side
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY > this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 5 && newY < this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}
			if (newX < -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > -this.FIELD_WIDTH / 2 + this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY > -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 5 && newY < -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}

			// Right Side
			if (newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY > this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 5 && newY < this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}
			if (newX < this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE + this.GOAL_POST_SIZE / 2 + 3 && newX > this.FIELD_WIDTH / 2 - this.GOAL_DISTANCE - this.GOAL_POST_SIZE / 2 - 3)
			{
				if (newY > -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2 - 5 && newY < -this.GOAL_SIZE / 2 - this.GOAL_POST_SIZE / 2)
				{
					this.velocityY = -this.velocityY;
				}
			}
		}

		this.positionX = newX;
		this.positionY = newY;
	}
}

