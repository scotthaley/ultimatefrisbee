<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Neo v1.0 |  Responsive Admin Skin</title>
        
        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.css" rel="stylesheet">
        
        <!-- Boostrap Theme -->
        <link href="css/skin.css" rel="stylesheet">
        <link href="css/boostrap-overrides.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">
        
        <!-- Font Awesome-->
        <link href="assets/css/font-awesome.css" rel="stylesheet">
        
        <!-- Animate-->
        <link href="assets/css/animate-custom.css" rel="stylesheet">
        
        <!-- Bootstrap core JavaScript -->
        <script src="assets/js/jquery.js"></script>
        <script src="dist/js/bootstrap.js"></script>
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->

  </head>
  <body class="layout-bg-med">

        <div id="login" class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 col-xs-offset-1">
                    <div class="center-panel animated fadeInDown">
                        <form class="form-signin form-group" action="scripts/submit_register.php" method="POST">
                            <h1 class="text-center">Sign Up</h1>
                            <br>
                            <!-- Display Name -->
                            <div class="row">
                            	<div class="col-xs-12">
                                	<div class="input-group">
                                      <span class="input-group-btn">
                                        <button disabled class="btn btn-default" type="button"><i class="icon-user text-transparent"></i></button>
                                      </span>
                                      <input type="text" name="display" class="form-control" placeholder="Display Name" value="<?php echo $_GET["display"]; ?>"required autofocus>
                                    </div><!-- /input-group -->
                     			</div>
                            </div><!-- / name -->
                            
                            <!-- Email -->
                            <div class="row">
                            	<div class="col-xs-12">
                                	<!-- input-group -->
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button disabled class="btn btn-default" type="button"><i class="icon-envelope text-transparent"></i></button>
                                      </span>
                                      <input type="text" name="email" class="form-control" placeholder="Email" value="<?php echo $_GET["email"]; ?>" required autofocus>
                                    </div><!-- /input-group -->
                     			</div>
                            </div><!-- / email -->
                            
                            <!-- Password -->
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <!-- input-group -->
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button disabled class="btn btn-default" type="button"><i class="icon-key text-transparent"></i></button>
                                      </span>
                                      <input type="password" name="password" class="form-control" placeholder="Password" required autofocus>
                                    </div><!-- /input-group -->
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <!-- input-group -->
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button disabled class="btn btn-default" type="button"><i class="icon-key text-transparent"></i></button>
                                      </span>
                                      <input type="password" name="repassword" class="form-control" placeholder="Retype Password" required autofocus>
                                    </div><!-- /input-group -->
                                </div>
                            </div><!-- / password -->
                           <br>
                           <div class="row">
                                    <div class="col-sm-12">
                                    <button class="btn btn-lg btn-primary btn-block animated flipInX" type="submit">
                                         Create a <strong>FREE</strong> account
                                    </button>
                            		</div>
                           </div>
                        </form>
                        <div class="alert alert-danger" <?php if ($_GET["d"] == "") { echo 'style="display:none";'; } ?>>
                            Display name must be between 5 and 15 characters.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["dan"] == "") { echo 'style="display:none";'; } ?>>
                            Display name must be alphanumeric.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["e"] == "") { echo 'style="display:none";'; } ?>>
                            Not a valid email address.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["pl"] == "") { echo 'style="display:none";'; } ?>>
                            Password must be between 5 and 20 characters.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["pan"] == "") { echo 'style="display:none";'; } ?>>
                            Password must be alphanumeric.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["pm"] == "") { echo 'style="display:none";'; } ?>>
                            Passwords do not match.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["eu"] == "") { echo 'style="display:none";'; } ?>>
                            This email address has already been used.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                        <div class="alert alert-danger" <?php if ($_GET["du"] == "") { echo 'style="display:none";'; } ?>>
                            This display name has already been used.
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <!-- Plugins & Custom -->
        <!-- Placed at the end of the document so the pages load faster -->
     
		<!-- Custom -->
		<script src="assets/js/script.js"></script>

  </body>
 </html>