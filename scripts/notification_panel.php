<?php

function echoNotifications()
{
	global $mysqli;

	$totalNotifications = 0;
	$notifications = array();

	$friendRequestResults = $mysqli->query("SELECT * FROM `Friend` WHERE `user2` = ".$_SESSION["id"]." AND `accepted` = 0");
	if ($friendRequestResults != false)
	{
		while ($fRequestRow = $friendRequestResults->fetch_assoc())
		{
			$userResult = $mysqli->query("SELECT * FROM `User` WHERE `id` = ".$fRequestRow["user1"]);
			if ($userResult != false)
			{
				$userRow = $userResult->fetch_assoc();
				$totalNotifications ++;
				$newNotification = new Notification("icon-user", $userRow["name"]." has sent you a friend request!", 
											"scripts/friend_request_respond.php?id=".$fRequestRow["id"]);
				array_push($notifications, $newNotification);
			}
		}
	}

	echo '<li class="dropdown">';
        echo '<a href="#" class="dropdown-toggle announcements" data-toggle="dropdown">';
    	if ($totalNotifications > 0)
        	echo '<i class="icon-flag-alt"></i><span class="badge animated-delay flash">'.$totalNotifications.'</span></a>';
        else
        	echo '<i class="icon-flag-alt"></i></a>';
        echo '<ul class="dropdown-menu">';
        if ($totalNotifications > 0)
        	echo '<li class="dropdown-header header-inverse"><i class="icon-star"></i>'.$totalNotifications.' Notifications </li>';
        else
        	echo '<li class="dropdown-header header-inverse"><i class="icon-star"></i>No Notifications </li>';

        foreach ($notifications as $notice)
		{
			$notice->Draw();
		}

        echo '</ul>';
    echo '</li>';
}

class Notification
{
	private $icon;
	private $text;
	private $link;

	function __construct($icon, $text, $link)
	{
		$this->icon = $icon;
		$this->text = $text;
		$this->link = $link;
	}

	function Draw()
	{
		echo '<li class="notification">';
            echo '<a href="'.$this->link.'">';
            echo '<span class="pull-left"><span class="label label-success"><i class="'.$this->icon.'"></i></span>  '.$this->text.'</span>';
            echo '<span class="time pull-right text-transparent"><i class="icon-times"></i></span><br>';
            echo '</a>';
        echo '</li>';
	}
}

?>