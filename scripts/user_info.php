<?php

function getUserLevel()
{
	echo $_SESSION["level"];
}

function getUserLevelPercent()
{
	echo $_SESSION["level"] * 10;
}

function getUserXP()
{
	echo $_SESSION["xp"];
}

function getUserXPPercent()
{
	echo $_SESSION["xp"] / 100;
}

function getProfilePicture()
{
	$email = $_SESSION["email"];
	$default = "http://www.somewhere.com/homestar.jpg";
	$size = 40;
	$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
	echo $grav_url;
}

function getProfilePictureFromEmail($email)
{
	$default = "http://www.somewhere.com/homestar.jpg";
	$size = 40;
	$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
	return $grav_url;
}

function getAcceptedFriends()
{
	global $mysqli;
	$friendResults = $mysqli->query("SELECT * FROM `Friend` WHERE (`user1` = ".$_SESSION["id"]." OR `user2` = ".$_SESSION["id"].") AND `accepted` = 1");
	if ($friendResults != false)
	{
		$friends = "";
		$first = "";
		while ($fRow = $friendResults->fetch_assoc())
		{
			if ($fRow["user1"] == $_SESSION["id"])
				$friends .= $first.$fRow["user2"];
			else
				$friends .= $first.$fRow["user1"];

			$first = ",";
		}

		return $friends;
	}
}

function getNotAcceptedFriends()
{
	global $mysqli;
	$friendResults = $mysqli->query("SELECT * FROM `Friend` WHERE (`user1` = ".$_SESSION["id"]." OR `user2` = ".$_SESSION["id"].") AND `accepted` = 0");
	if ($friendResults != false)
	{
		$friends = "";
		$first = "";
		while ($fRow = $friendResults->fetch_assoc())
		{
			if ($fRow["user1"] == $_SESSION["id"])
				$friends .= $first.$fRow["user2"];
			else
				$friends .= $first.$fRow["user1"];

			$first = ",";
		}

		return $friends;
	}
}

?>