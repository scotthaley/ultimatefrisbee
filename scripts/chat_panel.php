<?php

function echoChatPanelElements()
{
	global $mysqli;

	$totalFriends = 0;
	$friends = array();

	$friendRequestResults = $mysqli->query("SELECT * FROM `Friend` WHERE (`user1` = ".$_SESSION["id"]." OR `user2` = ".$_SESSION["id"].") AND `accepted` = 1");
	if ($friendRequestResults != false)
	{
		while ($fRequestRow = $friendRequestResults->fetch_assoc())
		{
			if ($fRequestRow["user1"] != $_SESSION["id"])
				$userResult = $mysqli->query("SELECT * FROM `User` WHERE `id` = ".$fRequestRow["user1"]);
			else
				$userResult = $mysqli->query("SELECT * FROM `User` WHERE `id` = ".$fRequestRow["user2"]);
			if ($userResult != false)
			{
				$userRow = $userResult->fetch_assoc();
				$totalFriends ++;
				$newChatElement = new ChatElement($userRow["name"], getProfilePictureFromEmail($userRow["email"]), $userRow["id"]);
				array_push($friends, $newChatElement);
			}
		}
	}

    foreach ($friends as $f)
	{
		$f->Draw();
	}

}

class ChatElement
{
	private $name;
	private $picture;
	private $id;

	function __construct($name, $picture, $id)
	{
		$this->name = $name;
		$this->picture = $picture;
		$this->id = $id;
	}

	function Draw()
	{
        echo '<li id="chatLiItem-'.$this->id.'" class="idle">';
        	echo '<a href="#" class="pull-left"><img src="'.$this->picture.'" alt="User" class="img-circle user-thumbnail-xs"></a>';
        	echo '<small><a href="#">'.$this->name.'<span class="pull-right"><i id="chatIcon-'.$this->id.'" class="icon-circle"></i></span></a></small>';
            echo '<div class="clearfix"></div>';
        echo '</li>';
	}
}

?>