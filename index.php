<?php
require("scripts/php_files.php");
?>

<!DOCTYPE html>
<html lang="en">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Ultimate Frisbee</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">

    <head>
    <!-- Boostrap Theme -->
    <link href="css/skin.css" rel="stylesheet">
    <link href="css/boostrap-overrides.css" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
        
    <!-- Add custom CSS here -->
    <link href="assets/css/sidebar.css" rel="stylesheet">
    
    <!-- Plugins -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/ezmark.css">
    <link href="assets/plugins/morris.js-0.4.3/morris.css" rel="stylesheet">
    <link href="assets/css/animate-custom.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/plugins/calendar/zabuto_calendar.css">
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css">
	<link href="assets/css/pushy.css" rel="stylesheet">

    <!-- Javascript -->
    <script src="assets/js/jquery.js"></script>
    <script src="http://99.136.94.105:8000/socket.io/socket.io.js"></script>
    <script src="dashboardManager.js"></script>

   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
	
    
</head>
<body onload="connectToServer(<?php echo $_SESSION["id"].",'".getAcceptedFriends()."'"; ?>)">
    <!-- Slide out chat panel -->
    <nav class="pushy pushy-right">
        <div class="well-lg no-radius">
            <i class="icon-group"></i>&nbsp;&nbsp;&nbsp;<strong>Chat</strong>&nbsp;&nbsp;<span class="text-transparent">(4)</span>
            <a href="javascript:;" class="close-chat-panel" data-dismiss="panel" aria-hidden="true"><i class="icon-remove pull-right text-transparent"></i></a>
			<br><br><div class="input-group inverse input-group-sm no-padding">
                      <span class="input-group-addon"><i class="icon-search text-transparent"></i></span>
                      <input type="text" class="form-control" placeholder="Search">
            </div>
           <div class="clearfix"></div>
        </div>
        <ul class="chat align-center-vert-alt">
            <li>
            	<a href="#" data-toggle="modal" data-target="#addFriendModal"><i class="icon-user"></i></a>
            	<small><a href="#" data-toggle="modal" data-target="#addFriendModal"> Add Friend<span class="text-online pull-right"></span></a></small>
                <div class="clearfix"></div>
            </li>
            <hr>
            <div id="chatListPanel">
                <?php
                echoChatPanelElements();
                ?>
            </div>
        </ul>
    </nav>


  <!-- Container for chat panel push-->
  <div id="container"> 
   <!-- Top Navbar  -->
        <div class="navbar navbar-static-top " role="navigation">
            <div class="navbar-header navbar-inverse text-center">
                <button type="button" class="navbar-toggle border-left-med no-radius" id="menu-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle border-left-med no-radius" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="avatar"><img src="img/avatar.jpg" alt="Ryan" class="img-circle"></span>
                </button>
                <!-- logo -->
                <div class="navbar-brand"><a href="index.html"> <img src="img/logo.png" alt="logo"></a></div>
                <!-- / logo -->
            </div>
            <!-- user -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right navbar-user">
                    <!-- alerts -->
                    <?php
                    echoNotifications();
                    ?>

                    <li class="dropdown">
                    	<a href="#"><div class="menu-btn"><i class="icon-comment-alt"></i><span class="badge">2</span></div></a>
                      
                    </li>
                    <li class="avatar dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php getProfilePicture(); ?>" alt="Ryan" class="img-circle nav-avatar">Welcome <?php echo $_SESSION["name"]; ?>!<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="user-profile.html"><i class="icon-user"></i> Profile</a></li>
                            <li><a href="email.html"><i class="icon-envelope"></i> Inbox</a></li>
                            <li><a href="#"><i class="icon-gear"></i> Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="scripts/logout.php"><i class="icon-power-off"></i> Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- / nav-collapse -->
        </div>
        <!-- / top Navbar -->
        
		<!-- Wrapper for content below nav bar -->
        <div id="wrapper">
            <!-- Sidebar -->
            <?php require("navbar.php"); ?>
            <!-- /sidebar -->
          
          <!-- preloader -->
          <div class='preloader-wrapper'>
               <div class="preloader">
                   <img src="img/preloader.png" alt='loading image'/>
               </div>
		  </div><!-- / preloader -->

          <!-- Keep all page content within the page-content-wrapper -->
          <div id="page-content-wrapper" class=" animated-med-delay fadeInRight">
                    <!-- page content header -->
                    <div class="row add-padding">
                            <div class="pull-left">
                                <h1>Good Morning <?php echo $_SESSION["name"]; ?></h1>
                                <ol class="breadcrumb">
                                    <li class="active"><a href="index.html" class="text-transparent">Dashboard</a></li>
                            	</ol>
                            </div>
                    </div>
                    <!-- / page content header -->

                    <!-- Page Notifications -->
                    <!-- Friend Requests -->
                    <div class="alert alert-success"  <?php if ($_GET["fr"] != "5") { echo 'style="display:none";'; } ?>>
                        <strong>You and <?php echo $_GET["n"]; ?> are now friends!</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                    <div class="alert alert-danger"  <?php if ($_GET["fr"] != "4") { echo 'style="display:none";'; } ?>>
                        <strong>Friend request could not be accepted!</strong> Please contact developer.
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                    <div class="alert alert-danger"  <?php if ($_GET["fr"] != "3") { echo 'style="display:none";'; } ?>>
                        <strong>Friend request was not sent!</strong> You can't add yourself.
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                    <div class="alert alert-danger"  <?php if ($_GET["fr"] != "2") { echo 'style="display:none";'; } ?>>
                        <strong>Friend request was not sent!</strong> User does not exist.
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                    <div class="alert alert-success"  <?php if ($_GET["fr"] != "1") { echo 'style="display:none";'; } ?>>
                        <strong>Friend request sent!</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                    <div class="alert alert-danger"  <?php if ($_GET["fr"] != "0") { echo 'style="display:none";'; } ?>>
                        <strong>Friend request was not sent!</strong> Unknown error. Please contact developer.
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </div>
                    

                    <!--Page Content-->
                    <!-- Stats & Updates Row 1-->
                        <div class="row no-padding">
                        	<!--row 1, col 1-->
                            <div class="col-md-3 col-sm-6">
                             	<div class="panel panel-body-only panel-secondary-solid">
                               		<div class="panel-body">
                                        <div class="pull-left">
                                        	<div class="badge-circle daily-stat-left"><div class="big-text"><i class="icon-user"></i></div></div>							
                                        </div>
                                        <span id="playersOnline" class="big-text">0</span>
                                            <div class="stat text-transparent">
                                                 <h5>Players Online </h5>
                                            </div> 
                                    </div>
                                </div>
                            </div><!--/ row 1, col 1-->
                            <!--row 1, col 2-->
                            <div class="col-md-3 col-sm-6">
                                <a href="playnow.php">
                                 	<div class="panel panel-body-only panel-warning-solid">
                                   		<div class="panel-body">
                                            <div class="pull-left">
                                                <div class="badge-circle daily-stat-left"><div class="big-text"><i class="icon-flag-checkered"></i></div></div>                           
                                            </div>
                                            <span class="big-text">Play Now</span>
                                                <div class="stat text-transparent">
                                                    <h5>Find a Game </h5>
                                                </div>
                                        </div>
                                    </div>
                                </a>
                            </div><!--/ row 1, col 2-->
                            <!--row 1, col 3-->
                            <div class="col-md-3 col-sm-6">
                            	<div id="serverStatusPanel" class="panel panel-body-only panel-primary-solid">
                               		<div class="panel-body">
                                        <div class="pull-left">
                                        	<div class="badge-circle daily-stat-left"><div class="big-text"><i class="icon-signal"></i></div></div>							
                                        </div>
                                        <span id="serverStatusText" class="big-text">Searching...</span>
                                            <div class="stat text-transparent">
                                                 <h5>Server Status </h5>
                                            </div> 
                                    </div>
                                </div>
                            </div><!--/ row 1, col 3-->
                            <!--row 1, col 4-->
                            <div class="col-md-3 col-sm-6">
                            	<div class="panel panel-body-only panel-info-solid">
                               		<div class="panel-body">
                                        <div class="pull-left">
                                        	<div class="badge-circle daily-stat-left"><div class="big-text"><i class="icon-globe"></i></div></div>							
                                        </div>
                                        <span id="new-orders" class="big-text"></span>
                                            <div class="stat text-transparent">
                                                 <h5>Games in Progress </h5>
                                            </div> 
                                    </div>
                                </div>
                            </div><!--/ row 1, col 4-->
                        </div><!--/ row-->
                    <!--/ Stats & Updates Row 1-->
                    
                     <!-- Stats & Updates Row 2-->
                   		<!--row-->
                        <div class="row no-padding">
                        	<!--col 1-->
                            <div class="col-md-6">
                             	<div class="panel panel-body-only panel-white-solid">
                               		<div class="panel-body">
                                         	 <a href="#" class="pull-left text-transparent"><h5>Level</h5></a>
                                             <div class="pull-right text-transparent"><h4><?php getUserLevel(); ?>/10</h4></div>
                                             <div class="clearfix"></div>
                                             <div class="progress progress-striped active">
                                                  <div class="progress-bar progress-bar-gray"  role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:<?php getUserLevelPercent(); ?>%"></div>
                                             </div>
                                    </div>
                                </div>
                            </div><!--/ col 1-->
                            <!--col 2-->
                            <div class="col-md-6">
                             	<div class="panel panel-body-only panel-white-solid">
                               		<div class="panel-body">
                                         	 <a href="#" class="pull-left text-transparent"><h5>Experience</h5></a>
                                             <div class="pull-right text-transparent"><h4><?php getUserXP(); ?>/1000</h4></div>
                                             <div class="clearfix"></div>
                                             <div class="progress progress-striped active">
                                                  <div class="progress-bar progress-bar-gray"  role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:<?php getUserXPPercent(); ?>%"></div>
                                             </div>
                                    </div>
                                </div>
                            </div><!--/  col 2-->
                        </div><!--/ row-->
                    <!--/ Stats & Updates Row 2-->
                    
                    <!--Stats & Updates Row 3 -->
                    <div class="row no-padding">
						<!-- Flot Realtime  -->
                                   <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="pull-left"><h4><i class="icon-sitemap"></i> Current Games</h4></div>
                                                <div class="tools pull-right">
                                                    <a href="#"><i class="icon-refresh text-transparent"></i></a>
                                                    <a href="javascript:;" class="close-panel" data-dismiss="panel" aria-hidden="true"><i class="icon-remove text-transparent"></i></a>
                                                 </div>
                                                 <div class="clearfix"></div>
                                            </div>
                                            <div class="panel-body no-padding">
                                            <!-- realtime chart  -->
                                            	<div id="content">
                                                    <div class="demo-container">
                                                        <div id="placeholder" class="demo-placeholder">No games currently.</div>
                                                    </div>
                                            	</div><!-- / realtime chart  -->
                                            
                                            </div>
                                            <div class="panel-footer">
                                            	<a href="#"><small>VIEW ALL</small></a> 
                                    			<a href="#"><div class="pull-right"><i class="icon-circle-arrow-right"></i></div></a> 
                                            </div>
                                       </div>
                                  </div>

                    </div><!--/ stats & updates row 3 -->
           </div><!-- /page-content-wrapper -->
		</div><!-- / wrapper for content below nav bar -->

	</div><!-- end container for chat panel push-->

    <!-- Modal dialog boxes / placed at the end to avoid mobile z-index conflict-->
    <div class="modal fade" id="addFriendModal" tabindex="-1" role="dialog" aria-hidden="true">
        <form class="form-signin form-group" action="scripts/friend_request.php" method="POST">
            <div class="modal-dialog">
              <div class="modal-content  brand-secondary-bg">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden=
                  "true">&#215;</button>
                </div>
        
                
                <div class="modal-body text-center">
                    <h2>Add Friend</h2>
                    <input type="text" name="display" class="form-control" placeholder="Display Name" required autofocus>
                </div>
        
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
                    <button type="submit" class="btn btn-primary">Send Request</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </form>
    </div><!-- /.modal -->

    <div class="modal fade blackout" id="gameFoundModal" tabindex="-1" role="dialog" aria-hidden="true">
        <form class="form-signin form-group" action="javascript:acceptGame();">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden=
                  "true">&#215;</button>
                </div>
        
                
                <div class="modal-body text-center">
                    <h2>Game Found!</h2>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-success" id="gameTimeoutBar" style="width:0%"></div>
                    </div>
                </div>
        
                <div id="gameFoundFooter" class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
                    <button type="submit" class="btn btn-primary">Enter Game!</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </form>
    </div><!-- /.modal -->

	
   	    <!-- JavaScript -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- Theme Core -->
		<script src="assets/js/jquery.js"></script>
    	<script src="assets/js/modernizr-2.6.2.min.js"></script>
        <script src="dist/js/bootstrap.js"></script>
        <script type="text/javascript" src="assets/js/jquery-ui-1.10.4.custom.min.js"></script>
        
		<script src="assets/js/script.js"></script>
        
        <!-- Pushy JS --> 
        <script src="assets/js/pushy.js"></script>
        
		<!-- Skycons --> 
		<script src="assets/js/skycons.js"></script>
        <script>
		  var icons = new Skycons(),
			  list  = [
				"clear-day", "clear-night", "partly-cloudy-day",
				"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
				"fog"
			  ],
			  i;
	
		  for(i = list.length; i--; )
			icons.set(list[i], list[i]);
	
		  icons.play();
		</script>
        
        <!-- Sparkline -->
		<script src="assets/js/jquery.sparkline.js"></script>
        
        <!-- Custom -->
		<script src="assets/js/sidebar-navbar.js"></script>

	    <!-- Preloader -->
		<script>
            $(window).load(function(){
             $('.preloader-wrapper').fadeOut();
            });
        </script>
        
</body>
</html>
