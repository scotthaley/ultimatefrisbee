<?php

$listItems = array();
$listButtons = array();

addCategory("Dashboard", "index.php", "icon-dashboard icon-2x");
addCategory("Stats", "stats.php", "icon-bar-chart icon-2x");

drawList();

function addCategory($label, $link="#", $icon)
{
	global $listItems;

	$newListObject = new ListObject($label, $link, $icon);
	array_push($listItems, $newListObject);
	return $newListObject;
}

function drawList()
{
	global $listItems, $listButtons;

	echo '<div id="sidebar-wrapper">';
	echo '	<ul class="sidebar-nav">';

	foreach ($listItems as $item)
	{
		$item->draw();
	}

	echo '</ul></div>';
}

class ListObject
{
	private $mListCategory;
	private $mListLink;
	private $mChildren = array();
	private $mIcon;

	function __construct($listCategory, $link, $icon)
	{
		$this->mListCategory = $listCategory;
		$this->mListLink = $link;
		$this->mIcon = $icon;
	}

	function addChild($label, $link, $get = null)
	{
		array_push($this->mChildren, new ListObjectChild($label, $link, $get));
	}

	function draw()
	{
		$current = false;
		if ($this->mListLink == basename($_SERVER['PHP_SELF']))
			$current = true;
		else
		{
			foreach ($this->mChildren as $child)
			{
				if ($child->isCurrent())
					$current = true;
			}
		}

		echo '<li>';
			if (!$current)
            	echo '<a href="'.$this->mListLink.'">';
            else
            	echo '<a href="'.$this->mListLink.'" class="active-title">';
            echo '<span class="nav-icon"><i class="'.$this->mIcon.'"></i></span>';
            echo '<span class="sidebar-menu-item-text">'.$this->mListCategory.'</span></a>';
            if (count($this->mChildren) != 0)
			{
				echo '<ul class="nav nav-list collapse submenu in" id="uiMenu">';
					foreach ($this->mChildren as $child)
					{
						$child->draw();
					}
                echo '</ul>';
			}
        echo '</li>';
	}
}

class ListObjectChild
{
	private $mLabel;
	private $mLink;
	private $mGetVariables;

	function __construct($label, $link, $get)
	{
		$this->mLabel = $label;
		$this->mLink = $link;
		$this->mGetVariables = $get;
	}

	function draw()
	{
		$getVars = "";
		foreach ($this->mGetVariables as $gName => $gVar)
		{
			if ($getVars == "")
				$getVars = "?";
			else
				$getVars .= "&";
			$getVars .= $gName."=".$gVar;
		}

		if ($this->isCurrent())
			echo '<li class="active"><a href="'.$this->mLink.$getVars.'">'.$this->mLabel.'</a></li>';
		else
			echo '<li><a href="'.$this->mLink.$getVars.'">'.$this->mLabel.'</a></li>';
	}

	function isCurrent()
	{
		if ($this->mLink == basename($_SERVER['PHP_SELF']))
		{
			if ($this->mGetVariables != null)
			{
				foreach ($this->mGetVariables as $gName => $gVar)
				{
					if ($_GET[$gName] != $gVar)
						return false;
				}
			}
			return true;
		}
		else
			return false;
	}
}
?>