<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Ultimate Frisbee Login</title>
        
        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.css" rel="stylesheet">
        
        <!-- Boostrap Theme -->
        <link href="css/skin.css" rel="stylesheet">
        <link href="css/boostrap-overrides.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">
        
        <!-- Ez mark-->
        <link rel="stylesheet" href="assets/css/ezmark.css">
        
        <!-- Font Awesome-->
        <link href="assets/css/font-awesome.css" rel="stylesheet">
        
        <!-- Animate-->
        <link href="assets/css/animate-custom.css" rel="stylesheet">
        
        <!-- Bootstrap core JavaScript -->
        <script src="assets/js/jquery.js"></script>
        <script src="dist/js/bootstrap.js"></script>
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->

  </head>
  <body class="layout-bg-med">

        <div id="login" class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-4">
                    <div class="col-sm-8 text-center">
                    	<div class="center-panel custom-check animated fadeInDown">
                                <span class="padding-bottom avatar avatar-md">
                                   <img src="img/UFShitLogo.png" alt="Ryan" width="100">
                                </span>
                                <form class="form-signin form-group" action="scripts/check_login.php" method="POST">
                                    <div class="input-stacked input-group-lg add-shadow">
                                            <input type="text" name="email" class="form-control" placeholder="Email" required autofocus>
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                    </div>
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">
                                         Sign in</button>
                                    <label class="checkbox pull-left">
                                        <input type="checkbox" value="remember-me">
                                        Remember me
                                    </label>
                                </form>
                                </br></br>
                                <div class="alert alert-danger" <?php if ($_GET["e"] == "") { echo 'style="display:none";'; } ?>>
                                    Incorrect username or password.
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                </div>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <a href="register.php" class="text-transparent"> Create an account </a>
                        	
                         
                     </div>   
                </div>
            </div>
        </div>
    
        <!-- Plugins & Custom -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- EzMark -->
        <script src="assets/js/jquery.ezmark.js"></script>
        <script type="text/javascript">
			$(function() {
				$('.custom-check input').ezMark()
			});	
		</script>
        
		<!-- Custom -->
		<script src="assets/js/script.js"></script>

  </body>
 </html>